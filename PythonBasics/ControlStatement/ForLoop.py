Lst=[10,20,30,40]

print("Accessing list members using for loop")
for l in Lst:
    print(l)

Name="Pranoday"

print("Printing string using for loop")
for ch in Name:
    print(ch,end='')

dict={"Name":"Pranoday","LastName":"Dingare",10:"Age",True:"Isemployeed"}
Ks=dict.keys()
print("Printing keys using for loop")
for K in Ks:
    print(K)
print("Printing dictionary values using keys  in for loop")
for K in Ks:
    print(dict[K])
#AllItems="Name":"Pranoday","LastName":"Dingare",10:"Age",True:"IsEmployeed"
AllItems=dict.items()

print("Printing items from dictionalry using for loop")
for Key,Value in AllItems:
    print(Key,Value)


print("Printing numbers using for loop and range function")
Numbers=range(1,11)
for num in Numbers:
    print(num)
num1=10
while num1 in Numbers:
    print("I am in while loop")
    num1=num1+1
num1=10000
while True:
    print("I am inside loop")
    if not num1 in Numbers:
        break;
    num1=num1+1