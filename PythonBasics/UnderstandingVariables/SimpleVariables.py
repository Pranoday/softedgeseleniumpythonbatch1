'''
    In Python we don't need to mention the data type of variable.
    e.g. in Java we define variable as,
    int num
    String Name

'''

num1="Pranoday"
print(num1)
num1=10
num2=20
Res=num1+num2
print(Res)

FirstName="Pranoday"
LastName="Dingare"
# + is a contcatenation operator
FullName=FirstName+" "+LastName
print(FullName)

Name="Pranoday"
Experience=10
Profession="Software Test Engineer"

#My name is Pranoday,I am Software Test Engineer having experience of 10 years
print("My name is "+Name+",I am "+Profession+" having experience of "+str(Experience)+" years")

IntroStatement="My name is {},I am {} having experience of {} years"
print(IntroStatement.format(Name,Profession,Experience))
'''
    format function accepts list of arguments.
    every argument can be accessed using its Index.
    Index starts with 0
'''
IntroStatement="My name is {2},I am {0} having experience of {1} years"
print(IntroStatement.format(Profession,Experience,Name))

'''

'''
IntroStatement="My name is {},I am  having experience of {} years"
MyIntro=IntroStatement.format(Profession,Experience,Name)

