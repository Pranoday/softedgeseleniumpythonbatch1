Data=["Pranoday",10,10.0,True,"Pune","India"]

'''
        Element:    Pranoday    10  10.0    True    Pune    India
        Index:      0           1   2       3       4       5

Slicing Notation:[Start:Stop:Step]
    Start is INCLUSIVE
    Stop is EXCLUSIVE
    Step is 1 by default
'''

print(Data[0])
#Data[Start:End:Step]
print(Data[0:3])        #O/P:['Pranoday', 10, 10.0]
print(Data[1:5])        #O/P:[10, 10.0, True, 'Pune']
#If we ommit Start then it take 0 by default and ommit End then it takes last Index by default
print(Data[:])   #O/P:['Pranoday', 10, 10.0, True, 'Pune', 'India']
#It will print 1st 4 elements
print(Data[:4]) #O/P:['Pranoday', 10, 10.0, True]

print(Data[len(Data)-2:])   #O/P:['Pune', 'India']
#It will print blank list [],if start is out of the range,it will not give any error
print(Data[8:10])
#It will print list till the last index if End is out of range,will not give any error
print(Data[1:10])
#It will start retrieving elements from Index 1 and will give every 2nd element from there
#i.e. Element at index 1,3(2nd from 1),5(2nd from 3) like that
print(Data[1::2])   #O/P:[10, True, 'India']

Data=["Pranoday",10,10.0,True,"Pune","India"]
print("Positive step",Data[::1])

#Reversing the list is possible by giving Negative step
print("Negative step",Data[::-1])
'''
        Element:    Pranoday    10      10.0        True        Pune        India
        Index:      0           1       2           3           4           5
Negative Index:     -6          -5      -4          -3          -2          -1
'''

'''
    Step defines from a given index retrieval will go towards RIGHT hand side or towards Left-hand side
    Positive step will start the retrieval from START index and go towards RIGHT hand side
    e.g.[1:4]  starting from index 1 to index 3 as 4 is exclusive
    
    Negative indexing will start the retrieval from START index and go towards LEFT hand sides 
    
'''
print("Start is BIGGER than end",Data[4:1:-1])  #O/P:['Pune', True, 10.0]
#   Pranoday    10      10.0        True        Pune        India
print(Data[2::1])
print(Data[0::3])

nums = [10, 20, 30, 40, 50, 60, 70, 80, 90]
'''
    0   1   2   3   4   5   6   7   8
    10  20  30  40  50  60  70  80  90
    -9  -8  -7  -6  -5  -4  -3  -2  -1
    
'''
print(nums[-2::-1])    #O/P:[80, 70, 60, 50, 40, 30, 20, 10]
print(nums[-2::1])     #O/P:[80,90]
print(nums[-2:1:-1])   #O/P:[80, 70, 60, 50, 40, 30]
print(nums[-2:1:1])    #O/P []

Name="Pranoday"
print(Name[::-1])