'''

    In python:
        list is a dynamic in nature.Meaning we can ADD,DELETE members from list at runtime
        List is defined using Square Bracket i.e.[
'''
NamesOfStudents=["Pranoday","Rajani","Anuja","Pushkar","Shikhar"]
print(len(NamesOfStudents))

print("Name of 1st student",NamesOfStudents[0])
print("Name of last Student",NamesOfStudents[4])
print("Name of last student",NamesOfStudents[len(NamesOfStudents)-1])
print("Original list : ",NamesOfStudents)
#append adds the new member at the end of existing members
NamesOfStudents.append("Snehal")
print("Contents of list after appending ",NamesOfStudents)

#insert operation inserts new element at specific index and existing elements
#present at the right hand swide on newly added element will shift their indexes
NamesOfStudents.insert(2,"Sneha")
print("Contents after inserting new member",NamesOfStudents)

#This will replace existing element at a specific index with new element
NamesOfStudents[0]="Vaishnavi"
print("List contents after updating ",NamesOfStudents)

#in operator checks for the existence of an element in a collection
#if element is present in operator return True else it returns False
print("Pranoday" in NamesOfStudents)
NamesOfStudents.append("Pranoday")
print(NamesOfStudents)
#index function will check for an element in list and will return the index at which element gets found
#If element is not present then it will return Error
try:
    Idx=NamesOfStudents.index("Pranoday")
    print(Idx)
    NamesOfStudents[Idx] = "Dingare"
except ValueError:
    print("Element is not present in the list")


#pop will remove the element from given index and it will return the element removed
print("pop returns the removed element.Element removed is ",NamesOfStudents.pop(6))

print("List after removing element at index 6",NamesOfStudents)
#remove function removes the particular element from list,it returns None
#If the element to be removed is not found in the list then remove function gives error
#NamesOfStudents.remove("PD")
#If we do not mention index to the pop then last element gets removed
NamesOfStudents.pop()
#if not "Pranoday" in NamesOfStudents:
 #   NamesOfStudents.append("Pranoday")
Name="Pranoday"
print("My name is",Name)

Lst=[1000,2000,5000]
print(Lst)
#clear method clears the contents but keep the variable alive so that if we wish to add new data
#we would be able to add in the same list
Lst.clear()
print(Lst)
Lst.append(10000)

#del will not only delete the contents but will also delete the variable from memory.We can not longer
#use it anymore unless we redefine it.
del Lst
Lst=[]
Lst.append(10000)