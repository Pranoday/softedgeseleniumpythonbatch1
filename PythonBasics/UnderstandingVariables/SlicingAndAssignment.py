nums = [10, 20, 30, 40, 50, 60, 70, 80, 90]
print(nums[0])

Newlist=nums[1:4]
print(Newlist)

Newlist1=[1,2,3,4,5,6,7]
#Updating a slice with new contents instead of updating using multiple assignment statements
Newlist1[0:4] = [100, 200, 300, 400]


'''
Newlist1[0]=100
Newlist1[1]=200
Newlist1[2]=300
Newlist1[3]=400
'''
print(Newlist1) #O/P: [1000, 2000, 5, 6, 7]

'''
    If number of elements in the slice is equal to the number of elements in the list we are
    having on the RIGHT SIDE of = sign,
    then each element from the slice gets replaced with corresponding element from the RIGHT side 
    list

    If number of elements in the slice is MORE than number of elements in list present on RIGHT
    side of = sign,
    then those slice members which more in number get replaced with less members from list present
    on RIGHT side of = sign 
    
    If slice has LESS number of elements than list present on RIGH side of = sign
    then  elements from slice gets replaced with CORRESPONDING elements from list present on RIGHT
    side of = sign,and remaining extra elements get inserted 
'''
Newlist1=[1,2,3,4,5,6,7]
Newlist1[0:4]=[1000,2000]
print(Newlist1)     #O/P:[1000, 2000, 5, 6, 7]

Newlist1=[1,2,3,4,5,6,7]
Newlist1[0:2]=[1000,2000,3000,4000,5000]  #O/P:[1000, 2000, 3000, 4000, 5000, 3, 4, 5, 6, 7]
print(Newlist1)

Newlist1=[1,2,3,4,5,6,7]
#Elements are getting removed
Newlist1[0:2]=[]
print(Newlist1)     #O/P:[3, 4, 5, 6, 7]

Newlist1=[1,2,3,4,5,6,7]
del Newlist1[2:6]
print(Newlist1)

