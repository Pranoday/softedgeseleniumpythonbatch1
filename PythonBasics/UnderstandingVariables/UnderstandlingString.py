Name="PranodayD"
'''
    P   r   a   n   o   d   a   y   D
    0   1   2   3   4   5   6   7   8
'''
print("First character from String ",Name[0])
print("First character from String ",Name[len(Name)-1])
#Strings are immutable,means we can not change contents of String
#Name[0]="p"

Name=" Pranoday "
#strip function removes spaces which are leading and trailing
print(Name.strip())
Name=" Pranoday Dingare "
#strip does not remove spaces which are present in between
print(Name.strip())

Name=" Pranoday Dingare "
print(Name.lstrip())

Name=" Pranoday Dingare "
print(Name.rstrip())

Name=" Pran oday "
'''
    index will start searching for 'a' from Left hand side and will return index of 1st occurence
'''
print(Name.index("a"))
#print(Name.index("z"))
'''
    rindex will search from Right hand side and will return the index of the 1st occurence
'''
print(Name.rindex("a"))

Name=" Sarat Pradhan "
print(Name.index("a"))
print(Name.rindex("a"))

Names=["Pranoday","Shikhar","Sarat"]
print(Names.index("Shikhar"))

#index function will start looking for char a from Index:4 onwards
Name="Pranoday Dingare"
print(Name.index("a",4))

Name="Pranoday"
print("'a' appears in String for ",Name.count("a"),"times")

#find function checks for the existence of a character or substring and it returns index of 1st occurence
#If not found it returns -1
print(Name.find("z"))

Statement=" My name is Pranoday "
pieces=Statement.strip().split(" ")
print(pieces)

Statement="My name is Pranoday. I leave in Pune"
print(Statement.split(" "))

#replace will not modify contents of original string
print(Name.replace("a","A"))
print("Original string ",Name)
#It will add leading and trailing spaces to make a string of required length
print(Name.center(11))
#endswith returns True if string ends with required chars else returns False
print(Name.endswith("day"))
#startswith returns True if string starts with required chars else returns False
print(Name.startswith("Pr"))

