import pytest

from CalculatorAPI.CalculatorAPI import Calculator

@pytest.mark.RegressionTest
def testSubtractionwithPositiveValues():
#def testSubtractionTestcase1():
    Obj=Calculator()
    res=Obj.Subtraction(50,20)
    assert res==30,"Subtraction does not work with postive values"


@pytest.mark.Sanity
def testSubtractionwithOnePositiveAndOneNegativeValue():
#def testSubtractionTestcase1():
    Obj=Calculator()
    res=Obj.Subtraction(-50,10)
    assert res==-60,"Subtraction does not work with 1 postive value and 1 negative value"

