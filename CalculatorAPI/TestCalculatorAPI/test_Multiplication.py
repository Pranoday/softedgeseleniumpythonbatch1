import pytest


#Obj=None
@pytest.mark.usefixtures("CreateCalcualtorObj1")
class TestMultiplication():

    Calc=None
    '''
        @pytest.fixture()
        def CreateCalculatorObj(self):
            print("In Create Obj")
            TestMultiplication.Obj=Calculator()
            yield
            print("Setting object to None")
            TestMultiplication.Obj=None
            #global Obj
            #Obj = Calculator()
        '''

    def testMultiplicationWithPositiveValues(self):
        res=TestMultiplication.Calc.Multiplication(10,20)
        #res = Obj.Multiplication(10, 20)
        assert 200==res,"Multiplication does not work with Positive values"

    def testMultiplicationWithZeo(self):
        res = TestMultiplication.Calc.Multiplication(100, 0)
        #res = Obj.Multiplication(100, 0)
        assert 0 == res, "Multiplication does not work with 0 value"