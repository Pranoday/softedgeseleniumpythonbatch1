'''

    Pytest is a Unit testing framework available on Python environment
    Unit testing tool executes different Unit test cases and generates a report
    Along with executing different test cases Unit testing tools manage execution of pre-requisites.

    Pytest considers files names of which either starts with test_ OR ends with _test
    as testcase files

    Functions names of which starts with word "test" are considered as testcase functions

    In order to run the testcases we need to be inside a directory having testcase files
    and run below command:

    pytest

    When we run this command Pytest scans the entire working directory to find filles
    having names starts either with test_ or ends with _test and runs functions having
    namess starting with word "test" from all these files

    If we want to run testcases from particular file then we have to run command:
    pytest <FileName>
    e.g. pytest test_Addition.py will run test functions from this file only

    If we want to run particular testcase function then we have to run below command:
    pytest -k <Full name of function> OR
    pytest -k <substring of functionname>
    e.g. pytest -k POSITIVE
    Command above will execute all test functions having word "POSITIVE" in ther names,
    from all test files

    We can create different groups of testcases based on type/scope of testing
    like RegressionTest,SmokeTest etc.

    We have to mark testcase function with @pytest.mark.RegressionTest for example.

    To run specific group of tests we need to run following command:
    pytest -m <GroupName>

    To run tests from group:RegressionTest,we need to execute:

    pytest --m RegressionTest
'''
import pytest


#Obj =None

'''

@pytest.fixture()
def CreateCalcualtorObj():
    #Obj=Calculator()
    global Obj
    Obj=Calculator()
    yield
    print("Going to execute after every function")
'''
@pytest.mark.RegressionTest
def testAdditionWithPositiveValues(CreateCalcualtorObj):
    #Obj=Calculator()
    #Res=Obj.Addition(10,20)
    Res=CreateCalcualtorObj.Addition(10,20)
    #This testcase function is a driver means it is a dummy which calls Addition(a unit under test)
    #It sends it required values and compares the result of Addition operation with
    #expected result using assertion
    assert Res==30,"Addition does not work with positive values"

@pytest.mark.SanityTest
def testAdditionwithOnePositiveOneNegativeNumbers(CreateCalcualtorObj):
    #Obj = Calculator()
    #Res = Obj.Addition(100, -20)
    Res = CreateCalcualtorObj.Addition(100, -20)
    # This testcase function is a driver means it is a dummy which calls Addition(a unit under test)
    # It sends it required values and compares the result of Addition operation with
    # expected result using assertion
    assert Res==80,"Addition does not work with 1 Positive and 1 Negative values"

@pytest.mark.SmokeTest
def testAdditionFunctionWithNegativeValues(CreateCalcualtorObj):

    #Obj = Calculator()
    #Res = Obj.Addition(-50, -20)
    Res=CreateCalcualtorObj.Addition(-50,-20)
    assert Res==-70,"Addition does not work with BOTH Negative values"
