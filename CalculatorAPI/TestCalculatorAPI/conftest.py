import pytest

from CalculatorAPI.CalculatorAPI import Calculator
#Obj=None

@pytest.fixture()
def CreateCalcualtorObj():
    #Obj=Calculator()
    Obj = Calculator()
    return Obj
'''
    To define a class level fixture we need to define fixture with scope class
    To set CLASS level variable to a value in fixture we have to say
    request.cls
    
    Here we are setting class level variable Obj to object of Calculator using
    request.cls.Obj=Object of Calculator
    
'''
@pytest.fixture(scope="class")
def CreateCalcualtorObj1(request):
    #Obj=Calculator()
    Obj = Calculator()
    request.cls.Calc=Obj


