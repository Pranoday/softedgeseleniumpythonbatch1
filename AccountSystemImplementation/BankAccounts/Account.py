from abc import ABC, abstractmethod
from typing import final, Final

from multipledispatch import dispatch

from BankAccounts.InsuffiecientBalanceException import InsufficientBalanceException


class Account(ABC):
    INTERESTRATE:Final=10.0
    '''
    def __init__(self,ActNo,Bal):
        print("I am in Account class Constructor")
        self.AccountNo=ActNo
        self.Balance=Bal
    '''
    @dispatch(int,float)
    def __init__(self, ActNo,B):
        self.AccountNo=ActNo
        self.Balance=B
    @dispatch(int)
    def __init__(self, ActNo):
        self.AccountNo = ActNo
        self.Balance = 0
    '''
    def __init__(self, *Args):
        print("I am in Account class Constructor")
        #self.AccountNo = ActNo
        #self.Balance = Bal
        if(len(Args)==2):
            self.AccountNo = Args[0]
            self.Balance = Args[1]
        elif(len(Args)==1):
            self.AccountNo = Args[0]
            self.Balance=0
        else:
            raise ("Account object can only be created either with 1 or 2 args")
    '''
    def DepositeAmount(self,AmountTobeDeposited):
        self.Balance=self.Balance+AmountTobeDeposited

    def WithdrawAmount(self, AmountTobeWithdrawn):
        #if self.Balance>AmountTobeWithdrawn:
         #   self.Balance = self.Balance - AmountTobeWithdrawn
        if self.Balance<AmountTobeWithdrawn:
            raise InsufficientBalanceException()
        self.Balance = self.Balance - AmountTobeWithdrawn
        Account.INTERESTRATE=20.0
    def GetBalance(self):
        return self.Balance

    @abstractmethod
    def CloseAccount(self):
        pass

    @dispatch(int,int)
    def sum(self,num1,num2):
        c=num1+num2
        print("Addition of 2 numbers is "+str(c))

    @dispatch(str,str)
    def sum(self,str1,str2):
        c = str1 + str2
        print("Contcatenation of 2 strings is " + c)

    @dispatch(str,str,str)
    def sum(self,str1,str2,str3):
        c = str1 + str2+str3
        print("Contcatenation of 3 strings is " + c)

    @dispatch(int,float)
    def sum(self,num1,num2):
        c=num1+num2
        print("Addition of 2 numbers Int and Float is "+str(c))

    @dispatch(int)
    def sum(self,a):
        print("Sum with 1 arg")

    @final
    def OpenAccount(self):
        print("Please submit KYC documents.Account opening may take 8 working days")