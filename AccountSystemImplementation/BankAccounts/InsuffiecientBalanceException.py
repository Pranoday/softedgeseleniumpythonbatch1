class InsufficientBalanceException(Exception):
    ExceptionMessage="You do not have sufficient amount in your account"
