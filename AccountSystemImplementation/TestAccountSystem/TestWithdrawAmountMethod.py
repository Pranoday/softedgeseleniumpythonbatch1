from TypesOfAccounts.SalaryAccount import SalaryAccount
''''
    Unit testing frameworks are used for:
    
    1.Calling test cases 
    2.Generating test case pass/fail report
    3.Providing assertions i.e. way to compare Actual and Expected behaviour
    4.Providing way to mention pre-requisite and post requisite using fixtures
    5.Provides us a way to do Data-driven tests 
    6.Providing drivers i.e. dummy to call a unit under test and provide with
    test data. 
'''
Sal=SalaryAccount(1)
Sal.DepositeAmount(1000)
Sal.WithdrawAmount(2000)